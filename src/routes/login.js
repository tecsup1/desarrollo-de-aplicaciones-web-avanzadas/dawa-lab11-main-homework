const express = require("express");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Usuario = require("../models/usuario");

const { OAuth2Client } = require("google-auth-library");
const e = require("express");
const client = new OAuth2Client(process.env.CLIENT_ID);

const app = express();
app.set("views", "./src/views"); // indicando donde está la carpeta view

// RUTA PARA IR A LA VISTA LOGIN:
app.get("/login", (req, res) => {
    res.render("login");
});

// RUTA PARA GENERAR TOKEN
app.post("/login", (req, res) => {
    let body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            return res.render("login", {
                msg: "Hubo un error en la petición.",
            });
        }

        if (!usuarioDB) {
            return res.render("login", { msg: "Credenciales inválidas1" });
        }

        if (!bcrypt.compareSync(body.password, usuarioDB.password)) { // por alguna razón esta comparación no funciona en local
        // if (body.password != usuarioDB.password) { // por alguna razón esta comparación no funciona en heroku
            res.render("login", { msg: "Credenciales inválidas2" });
        }

        let token = jwt.sign(
            {
                usuario: usuarioDB,
            },
            process.env.SEED,
            {
                expiresIn: process.env.CADUCIDAD_TOKEN,
            }
        );

        Usuario.find({}).exec((err, usuarios) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err,
                });
            }
            res.render("index", {
                usuarios: usuarios,
                token: token,
            });
            // return res.json({
            //     ok:true,
            //     usuario: usuarioDB,
            //     token,
            // })
        });

        // res.redirect('/usuario')
        // res.json({
        // ok: true,
        // usuario: usuarioDB,
        // token
        // });
    });
});

// lab10:
app.get("/", (req, res) => {
    res.render("index");
});

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();

    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);
    return payload;
}

app.post("/google", async (req, res) => {
    let token = req.body.idtoken;

    let googleUser = await verify(token).catch((e) => {
        return res.status(403).json({ ok: false, err: e });
    });

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({ ok: false, err });
        }

        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Debe de usar su autenticación normal",
                    },
                });
            } else {
                let token = jwt.sign({ usuario: usuarioDB }, process.env.SEED, {
                    expiresIn: process.env.CADUCIDAD_TOKEN,
                });
                // return res.json({ ok: true, usuario: usuarioDB, token });
                Usuario.find({}).exec((err, usuarios) => {
                    if (err) {
                        return res.status(400).json({ ok: false, err });
                    }
                    
                    // res.render("error", {
                    //     usuarios: usuarios,
                    //     token: token,
                    // });
                    return res.json({
                        ok:true,
                        usuario: usuarioDB,
                        token,
                        chalius: "que pasa prro",

                    })
                });
            }
        } else {
            //si el usuario no existe en nuestra base de datos
            let usuario = new Usuario();

            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.picture;
            usuario.google = true;
            usuario.password = "123";

            usuario.save((err, usuarioDB) => {
                if (err) {

                    return res.status(500).json({ ok: false, err });
                }

                let token = jwt.sign(
                    {
                        usuario: usuarioDB,
                    },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );

                // retornar vista:
                //return res.json({ ok: true, usuario: usuarioDB, token,});

                Usuario.find({}).exec((err, usuarios) => {
                    console.log("usuarios2");

                    if (err) {

                        return res.status(400).json({
                            ok: false,
                            err,
                        });
                    }

                    res.render("index", {
                        usuarios: usuarios,
                        token: token,
                        chalius: "que pasa prro",

                    });
                });
            });
        }
    });
});

module.exports = app;
