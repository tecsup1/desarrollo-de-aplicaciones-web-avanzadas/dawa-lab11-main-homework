

// puerto
process.env.PORT = process.env.PORT || 3000

// entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

// Vencimiento de token
// 60 segundos * 60 minutos * 24 horas * 30 días
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30

// SEED de autenticación:
process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo'


// base de datos
let urlDB;

if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb://localhost:27017/lab07DB'
}else{
    urlDB = 'mongodb+srv://chalius:rastachalius@cluster0-0sas4.mongodb.net/test'
}

process.env.URLDB = urlDB

// google client id:
process.env.CLIENT_ID = process.env.CLIENT_ID || '880776411615-v6k8qi4mur051v4i129ml7tf50fsh9td.apps.googleusercontent.com'